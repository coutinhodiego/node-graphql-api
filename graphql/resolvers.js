const User = require('../models/UserSchema')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')


exports.hello = () => {
    return {
        msg: 'Hello Word!',
        status: 200
    }
}

exports.sayHello = () => {
    return {
        msg: 'Just say hello!',
        status: 200,
        body: 'no have body dude.'
    }
}

exports.createUser = async ({ userInput }, req) => {

    const existingUser = await User.findOne({ "email": userInput.email })

    if (existingUser) {

        const error = new Error('user exists already!')
        throw error;
    }

    const hashPass = await bcrypt.hash(userInput.password, 12)
    const user = new User({
        name: userInput.name,
        email: userInput.email,
        password: hashPass
    })

    const createdUser = await user.save()

    return { ...createdUser._doc }

}

exports.login = async ({ email, password }) => {

    const user = await User.findOne({ email: email })

    if (!user) {
        const error = new Error('User not found.')
        error.code = 401
        throw error
    }

    const pass = await bcrypt.compare(password, user.password)
    if (!pass) {
        const error = new Error('Invalid credentials')
        error.code = 401
        throw error
    }

    const token = jwt.sign({
        userId: user._id,
        email: user.email
    }, 'secretsecretword',
        { expiresIn: '1h' }
    )

    return { token : token, userId : user._id}
}

exports.upDateUser = async ({ userInput }, req) => {
    
    const user = await User.findOne({email: userInput.email})

    if (!user) {
        const error = new Error('User not found.')
        error.code = 401
        throw error
    }

    user.name = userInput.name
    user.email = userInput.email

    const updatedUser = await user.save()

    return { ...updatedUser._doc }
}

exports.upDatePassword = async ({ userInput }, req) => {

    const user = await User.findOne({email: userInput.email})

    if (!user) {
        const error = new Error('User not found.')
        error.code = 401
        throw error
    }

    const hashPass = await bcrypt.hash(userInput.password, 12)
    user.password = hashPass

    const updatedUser = await user.save()

    return { ...updatedUser._doc }
}

exports.removeUser = async ({ email, password }) => {

    
    const user = await User.findOne({email: email})

    if (!user) {
        const error = new Error('User not found.')
        error.code = 401
        throw error
    }
    
    const pass = await bcrypt.compare(password, user.password)

    if (!pass) {
        const error = new Error('Invalid credentials')
        error.code = 401
        throw error
    }

    const removedUser = await User.findByIdAndRemove(user._id)

    return { ...removedUser._doc }
}