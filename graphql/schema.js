const { buildSchema } = require('graphql')
// const methods = require('./methods.gql')

module.exports = buildSchema(`
    type HelloData {
        msg: String!
        status: Int!
    }

    type SayHelloData {
        msg: String!
        status: Int!
        body: String!
    }

    type Token {
        token: String!
        userId: String!
    }

    type QueryMethods {
        hello: HelloData!
        sayHello: SayHelloData!
        login(email: String!, password: String!) : Token!
    }

    type User {
        _id: ID!
        name: String!
        email: String!
        password: String
        status: String!
    }

    input UserInputData {
        name: String!
        email: String!
        password: String!
    }
    
    input UpdateUser {
        name: String!
        email: String!
    }
    
    input UpdatePassword {
        email: String!
        password: String!
    }

    type MutationMethods {
        createUser(userInput: UserInputData) : User!
        upDateUser(userInput: UpdateUser) : User!
        upDatePassword(userInput: UpdatePassword) : User!
        removeUser(email: String!, password: String!) : User!
    }

    schema {
        query: QueryMethods
        mutation: MutationMethods
    }
`)