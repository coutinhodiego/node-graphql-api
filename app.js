const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const graphqlHttp = require('express-graphql')
const graphqlSchema = require('./graphql/schema')
const graphqlResolvers = require('./graphql/resolvers')

const app = express();

const PORT = process.env.PORT || 3000;
const MONGO_DB = process.env.MONGO_DB || 'mongodb://localhost/user-graphql-api'

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}))
app.use(bodyParser.json())
app.use(cors());

app.use('/', graphqlHttp({
    schema : graphqlSchema,
    rootValue: graphqlResolvers,
    graphiql: true

}));

mongoose.connect(MONGO_DB, { useNewUrlParser: true })
.then((result) => {

    //Abrindo conexao da aplicaçao
    app.listen(PORT, () => {
        let host = result.connections[0].host

        console.log(`DATA-BASE : ${host}`)
        console.log(`LISTEN ON PORT : ${PORT}`)
        console.log('GRAPHQL PROJECT')
    });
})
.catch(err => console.log(err))